macro_rules! html{ 
    ($o:expr,$string:expr) => {
      $o.eng.push(OutText::Html($string.to_string()));
    };
    ($o:expr,$fmt_string:expr, $($arg:expr),*) => {
      $o.eng.push(OutText::Html(format!($fmt_string,$($arg),*)));
    };
} 
macro_rules! plain{ 
    ($o:expr,$string:expr) => {
        $o.eng.push(OutText::Plain($string));
    };
    ($o:expr,$fmt_string:expr, $($arg:expr),*) => {
        $o.eng.push(OutText::Plain(format!($fmt_string,$($arg),*)));
    };
}