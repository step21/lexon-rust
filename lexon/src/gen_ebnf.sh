#!/bin/sh
#translate to more or less ebnf
sed -E \
-e 's/= *[_@]?\{/=/g' \
-e 's/\}/;/g' \
-e 's/\^"/"/g' \
-e 's/\~/,/g' \
-e 's/\((.+)\)\?/[\1]/g' \
-e 's/([^ ]+)\?/[\1]/g' \
-e 's/\((.+)\)\*/{ \1 }/' \
-e 's/\((.+)\)\+/{ \1 }/' \
lexon.pest > lexon.ebnf
