use chrono::prelude::*;
use crate::ast::*;
use crate::wallets::*;
use serde::Serialize;
use std::collections::HashMap;

#[derive(Debug,Clone,Serialize)]
pub enum Value{
    Id(usize),//hex blockchain id
    Date(DateTime<Utc>),
    Binary(Option<bool>),
    Integer(i32),
    ThisAddress,
    MsgValue
}

#[derive(Debug,Clone,Serialize)]
pub struct Variable{
    pub name:String,
    pub _type:Type,
    pub value:Option<Value>,
    pub bound:bool
}

#[derive(Debug,Clone,Serialize)]
pub struct Object{
    pub name:String,
    pub variables:Vec<Variable>
}

#[derive(Debug,Clone,Serialize)]
pub enum Operand{
  Val(Value),
  Var(String),
  Par(String),
  Exp(Expression),
  Func(String)
}

#[derive(Debug,Serialize)]
pub enum Operation{
  Assign(Operand,Operand),
  Sub(Operand,Operand),
  Add(Operand,Operand),
  Inc(Operand,Operand),
  Dec(Operand,Operand),
  Transfer{who:Option<Operand>,from:Option<Operand>,exp:Operand,to:Operand},
  Reveal(Vec<Operand>),
  Terminate,
  May(Operand,Vec<Operation>),
  If(BooleanExpression,Vec<Operation>,Vec<Operation>),
  Call(Operand,Vec<Operand>)
}

#[derive(Debug,Serialize)]
pub struct  Function{
  pub name:String,
  pub payable:bool,
  pub parameters:Vec<Variable>,
  pub variables: Vec<Variable>,
  pub operations:Vec<Operation>,
  pub is_private:bool,
  pub returns:Vec<Variable>,
  pub mays:Vec<String>
}

#[derive(Debug,Serialize)]
pub struct Program{
  //variables
  pub name:String,
  pub wallet:Option<usize>,//index to wallets
  pub comments:Vec<String>,
  pub payable:bool,
  pub variables:Vec<Variable>,
  pub objects:Vec<Object>,
  pub constructor:Function,
  pub terms_functions:Vec<Function>
}

#[derive(Debug,Serialize)]
pub enum OutText{
  Plain(String),
  Html(String)
}

#[derive(Debug,Serialize)]
pub struct Output{
  pub sol:Vec<String>,
  pub sop:Vec<String>,
  pub eng:Vec<OutText>
}

#[derive(Debug,Serialize)]
pub struct ExprOut{
  pub sol:String,
  pub sop:String,
  pub eng:String
}

#[derive(Debug,Serialize)]
pub struct LexonVM{  
  wallets:Wallets,
  lexons:Vec<Lexon>,
  pub programs:Vec<Program>
}


impl LexonVM {
  pub fn new(lexons:Vec<Lexon>)->LexonVM{
    
    let mut programs:Vec<Program>=Vec::new();
    let wallets=Wallets::new();
    //println!("2nd AST {:#?}",lexons );
    for lexon in &lexons{ 
      let mut comments:Vec<String>=Vec::new();
      let payable:bool;
      let mut parameters:Vec<Variable>=Vec::new();
      let mut variables:Vec<Variable>=Vec::new();
      let mut objects:Vec<Object>=Vec::new();
      let mut terms_functions:Vec<Function>=Vec::new();
      let mut operations:Vec<Operation>=Vec::new();
      let mut first_person=true;
      let mut first_amount=true;
    
      for contract in &lexon.contracts{
	println!("contract name x: {}", contract.name);
        let mut obj = Object{name:contract.name.to_string(),variables:Vec::new()};
        for stmts in &contract.statements{
          let statement_type=&stmts.stmt;
          match statement_type{
              Stmt::Definition{_type} => {
                  let name=stmts.varnames[0].to_lowercase().to_string();
                  
                  let var=match _type{
                      Type::UndefType=>{
                        Variable{_type:Type::UndefType,name,value:None,bound:false}
                      },
                      Type::Person => {
                          let mut bound=false;  
                          if first_person {
                            first_person=false;
                            bound=true;
                          }
                          Variable{_type:Type::Person,name,value:None,bound}
                      },
                      Type::Amount=>{
                        let bound=false;  
                        Variable{_type:Type::Amount,name,value:None,bound}
                      },
                      Type::Key=>{
                        Variable{_type:Type::Key,name,value:None,bound:false}
                      },
                      Type::Data=>{
                        Variable{_type:Type::Data,name,value:None,bound:false}
                      },
                      Type::Time=>{
                        Variable{_type:Type::Time,name,value:None,bound:false}
                      },
                      Type::Text=>{
                        Variable{_type:Type::Text,name,value:None,bound:false}
                      },
                      Type::Binary=>{
                        Variable{_type:Type::Binary,name,value:None,bound:false}
                      },
                      Type::Asset=>{
                        Variable{_type:Type::Asset,name,value:None,bound:false}
                      },
                      Type::Contract=>{
                        Variable{_type:Type::Contract,name,value:None,bound:false}
                      }                      
                  };
                  obj.variables.push(var);
              },
              _ => {}
          }
        }
        objects.push(obj);  
      }

      for stmts in &lexon.term_stmts{
          let stmt=&stmts.stmt;
          match stmt{
              Stmt::Definition{_type} => {
                  let name=stmts.varnames[0].to_lowercase().to_string();
                  
                  let var=match _type{
                      Type::UndefType=>{
                        Variable{_type:Type::UndefType,name,value:None,bound:false}
                      },
                      Type::Person => {
                          let mut bound=false;  
                          if first_person {
                            first_person=false;
                            bound=true;
                          }
                          Variable{_type:Type::Person,name,value:None,bound}
                      },
                      Type::Amount=>{
                        let bound=false;  
                        Variable{_type:Type::Amount,name,value:None,bound}
                      },
                      Type::Key=>{
                        Variable{_type:Type::Key,name,value:None,bound:false}
                      },
                      Type::Data=>{
                        Variable{_type:Type::Data,name,value:None,bound:false}
                      },
                      Type::Time=>{
                        Variable{_type:Type::Time,name,value:None,bound:false}
                      },
                      Type::Text=>{
                        Variable{_type:Type::Text,name,value:None,bound:false}
                      },
                      Type::Binary=>{
                        Variable{_type:Type::Binary,name,value:None,bound:false}
                      },
                      Type::Asset=>{
                        Variable{_type:Type::Asset,name,value:None,bound:false}
                      },
                      Type::Contract=>{
                        Variable{_type:Type::Contract,name,value:None,bound:false}
                      }                      
                  };
                  
                  variables.push(var);

              },
              _ => {}
          }
      }
      // check bound variables, and infer variables
      for stmts in &lexon.term_stmts{
          let stmt=&stmts.stmt;
          match stmt{              
              Stmt::With(name,expr)=>{ 
                if let Some(_e) = expr { //bind if there's an expression
                  let name=name.to_lowercase();
                  if let Some(v) = variables.iter().position(|x| x.name==name ){
                    variables[v].bound=true;
                  }else{
                    if _e.terms.len()==1 && _e.terms[0].factors.len()==1 {
                      match &_e.terms[0].factors[0]{
                        Factor::Time(_t)=>{                          
                          variables.push(Variable{_type:Type::Time,name:name.clone(),value:None,bound:true});
                        },
                        _=>{}
                      }
                    }
                  }                  
                }                
              },
              Stmt::Fix(expr)=>{ 
                if let Some(_e) = expr {
                  let name=stmts.varnames.last().unwrap().to_string().to_lowercase();
                  if let Some(v) = variables.iter().position(|x| x.name==name ){
                    variables[v].bound=true;
                  }else{
                    if _e.terms.len()==1 && _e.terms[0].factors.len()==1 {
                      match &_e.terms[0].factors[0]{
                        Factor::Time(_t)=>{  
                          variables.push(Variable{_type:Type::Time,name:name.clone(),value:None,bound:true});
                        },

                        _=>{}
                      }
                    }
                  }
                }                
              },
              Stmt::Certify(_s,list)=>{
                for c in list{
                  if let None = variables.iter().position(|x| &x.name==c ){                  
                    variables.push(Variable{_type:Type::Data,name:c.to_lowercase(),value:None,bound:false});
                  }
                }
              },
              Stmt::Be{def,expression}=>{
                  let name=stmts.varnames.last().unwrap().to_string().to_lowercase();
                  let v=variables.iter().position(|x| x.name==name );
                  match expression{
                    Some(_e)=>{
                      let name=name.to_lowercase();
                      if let Some(v) = variables.iter().position(|x| x.name==name ){
                        variables[v].bound=true;
                      }else{
                        if _e.terms.len()==1 && _e.terms[0].factors.len()==1 {
                          match &_e.terms[0].factors[0]{
                            Factor::Time(_t)=>{
                              variables.push(Variable{_type:Type::Time,name:name.clone(),value:None,bound:true});
                            },
                            _=>{}
                          }
                        }
                      }
                    }, 
                    _=>{
                      if v ==None {
                        match def{
                            Defs::Certified=>{                            
                              variables.push(Variable{_type:Type::Data,name:name.clone(),value:None,bound:false});                              
                            },
                            _=>{
                                
                            } 
                        }
                      }   
                    }
                  }
              },
              Stmt::May(who,_statements)=>{
                  if !variables.iter().any(|x| &x.name==who )
                  {
                      variables.push(Variable{_type:Type::Person,name:who.to_string(),
                                      value:None,bound:false});
                  } 
              },
              Stmt::Pay{who: _,from: _,exp,to} => { 
                  if to.sym=="escrow" {
                    let _name="escrow".to_string();              
                    //variables.push(Variable{_type:Type::Person,name,value:None,bound:false});
                  }
                  //TODO: improve expression traversing
                  if exp.terms.len()==1 && exp.terms[0].factors.len()==1 {
                      match &exp.terms[0].factors[0]{
                          Factor::Sym(sym)=>{
                              let sym=sym.sym.to_lowercase();
                              match sym.as_str(){
                                "Amount"=>{
                                  
                                  variables.push(Variable{_type:Type::Amount,name:sym,value:None,bound:true});
                                },
                                _=>{
                                  if let Some(v) = variables.iter().position(|x| x.name==sym ){ 
                                      match variables[v]._type {
                                        Type::Amount=>{
                                          if first_amount {
                                            first_amount=false;
                                            variables[v].bound=true;
                                            operations.push(
                                              Operation::Assign(
                                                Operand::Var(sym),
                                                Operand::Val(Value::MsgValue)
                                              )
                                            )
                                          }
                                        },
                                        _=>{}
                                      }
                                  }
                                }
                              }
                          },
                          _=>{}
                      }
                  }
              }
              _ => {}
          }
      }
      
      for comment in &lexon.comments{
        comments.push(comment.to_string());
      }
     
      payable = lexon.toplevel_payable;

      for var in &variables{
          if !var.bound && var.name!="escrow".to_string(){
              let mut v=var.clone();
              v.name=format!("_{}",var.name);
              parameters.push(v);
          } 
          if var.name=="amount".to_string() && first_amount{
            first_amount=false;
            operations.push(
              Operation::Assign(
                Operand::Var("amount".to_string()),
                Operand::Val(Value::MsgValue)
              )
            )
          }
          if var.name=="escrow".to_string(){
            //operations.push(
            //  Operation::Assign(
            //    Operand::Var("escrow".to_string()),
            //    Operand::Val(Value::ThisAddress)
            //  )
            //)
          }
      }
      
      let ctr_variables:Vec<Variable>=Vec::new();
      
      let mut mays:Vec<String>=Vec::new();
      let returns:Vec<Variable>=Vec::new();
      let is_private=false;
      vm_stmts(&lexon.term_stmts,&variables,&mut parameters,&mut operations,&mut mays);

      let name=lexon.name.clone();
      let constructor=Function{name,payable:lexon.term_payable,parameters,variables:ctr_variables,
	operations,mays,is_private,returns};
      
      
      for chpt in &lexon.term_chpts{
          
          let chpt_variables:Vec<Variable>=Vec::new();
          let mut chpt_parameters:Vec<Variable>=Vec::new();
          let mut chpt_operations:Vec<Operation>=Vec::new();

          let mut mays:Vec<String>=Vec::new();
          let mut returns:Vec<Variable>=Vec::new();
          let is_private=vm_stmts(&chpt.statements,&variables,&mut chpt_parameters,&mut chpt_operations,&mut mays);
          
          for r in &chpt.ret{
            if let Some(v) = variables.iter().position(|x| x.name==r.to_lowercase() ){
              returns.push(variables[v].clone());
            }
          }
          terms_functions.push(Function{
            name:chpt.name.clone(),
	    payable:chpt.payable,
            parameters:chpt_parameters,
            variables:chpt_variables,
            operations:chpt_operations,
            is_private,
            mays,
            returns
          });
      }
      
      let wallet = None;
      let name=lexon.name.clone();
      programs.push(Program{
        name,
        comments,
	payable,
        wallet,
        constructor,
        variables,
        objects,
        terms_functions
      });
    }
    
    LexonVM{wallets,programs,lexons}
  } 
  
  pub fn english(&self)->String{

    //self.output.eng.join("\n")
    
    /*self.output.eng.iter().fold(String::new(),|out,string|  
        out+ &match string{
          OutText::Plain(t)=>format!("{}\n",t ),
          OutText::Html(t) =>format!("{}\n",t )
        }
    )*/
    "".to_string()
  }
  pub fn get_json(&self)->String{
    match serde_json::to_string_pretty(&self.lexons){
        Ok(s) => s,
        Err(e) => format!("{:?}",e )
    }
  }
  pub fn get_args(&self)->String{
    let mut out=HashMap::new();
    //let out:Vec<>=Vec::new();
    for prog in &self.programs{
      let mut params:Vec<HashMap<String,String>>=Vec::new();
      for arg in &prog.constructor.parameters{
        let mut param=HashMap::new();
        param.insert("name".to_string(),arg.name.clone());
        param.insert("type".to_string(),arg._type.to_string());
        params.push(param);
      }
      out.insert(prog.constructor.name.clone(),params);
    };
    match serde_json::to_string_pretty(&out){
        Ok(s) => s,
        Err(e) => format!("{:?}",e )
    }
  }
}

fn vm_stmts(    statements: &Vec<Statement>,
                 variables: &Vec<Variable>,
                mut parameters: &mut Vec<Variable>,
                operations: &mut Vec<Operation>,
                  mut mays: &mut Vec<String>
  )->bool //return true if is private
{
  let mut ret=true;
  for stmts in statements{
      let stmt=&stmts.stmt;
      match stmt{
          Stmt::Pay{who,from,exp,to} => { 

              let who=match who{
                  Some(p)=>{
                    Some(Operand::Var(p.sym.to_string()))
                  },
                  None=>None
              };
              let from=match from{
                  Some(p)=>{
                    Some(Operand::Var(p.sym.to_string()))
                  },
                  None=>None
              };
              
              let mut name=to.sym.to_lowercase();
              //println!("pay to: {:#?}",name );
              if name == "themselves" {
                //println!("mays {:#?}",mays );
                if mays.len()>0 {
                  name=mays.last().unwrap().to_string();
                }
              }

              let to=if to.given {
                parameters.push(
                    Variable{
                        name:name.clone(),
                        _type:Type::Person,
                        value:None,
                        bound:false
                    }
                );
                Operand::Par(name)
              }else{ 
                Operand::Var(name) 
              };
              vm_params_from_expr(exp,variables,parameters);

              operations.push(
                  Operation::Transfer{
                      who,
                      from,
                      exp:Operand::Exp(exp.clone()),
                      to
                  }
              );
              /*
              match stmts.varnames.len(){
                1=>{
                    if to.sym=="themselves"{
                      if mays.len()>0 {//if we are in a may
                        let may_who=mays.last().unwrap().to_string();
                        to=may_who.clone().to_lowercase();
                      }else{
                        //???
                      }
                    }
                    operations.push(
                        Operation::Transfer(
                            Operand::Var(to.to_string()),
                            Operand::Exp(expr.clone())
                        )
                    )
                },
                2=>{
                    if mays.len()>0 {//if we are in a may
                        let may_who=mays.last().unwrap().to_string();
                        let mut dest=String::new();
                        if stmts.varnames[1]=="themselves"{
                            dest=may_who.clone().to_lowercase();
                        }else {
                          dest=stmts.varnames[1].to_lowercase();
                        }
                        operations.push(
                            Operation::Transfer(
                                Operand::Var(dest.to_string()),
                                Operand::Exp(expr.clone())
                            )
                        )
                    }
                },
                _=>{}
              }
              */
          },
          Stmt::Return(what,to)=> {
              let mut name=to.sym.to_lowercase();
              if name == "themselves" {
                //println!("mays {:#?}",mays );
                if mays.len()>0 {
                  name=mays.last().unwrap().to_string();
                }
              }
              let to=if to.given {
                Operand::Par(name)
              }else{ 
                Operand::Var(name) 
              };
              for w in what {
                  operations.push(
                      Operation::Transfer{
                          who:None,
                          from:None,
                          exp:Operand::Exp(w.clone()),
                          to:to.clone()                   
                      }
                  )
              }
          },
          Stmt::With(dest,expr)=>{
              match expr{
                  Some(ex)=>{
                      operations.push(
                        Operation::Assign(
                          Operand::Var(dest.to_lowercase()),
                          Operand::Exp(ex.clone())
                        )
                      )
                  },
                  None=>{}
              }
          },
          Stmt::Be{def,expression}=>{
              let what=stmts.varnames.last().unwrap().to_string().to_lowercase();
              match def{
                  Defs::Fixed|Defs::Certified|Defs::Appointed|Defs::Notified=>{                  
                      if let Some(ex) = expression {
                        operations.push(
                          Operation::Assign(
                            Operand::Var(what),
                            Operand::Exp(ex.clone())
                          )
                        )                        
                      } else {                        
                        operations.push(
                          Operation::Assign(
                            Operand::Var(what.to_string()),
                            Operand::Par(format!("_{}",what))
                          )
                        )
                      }
                  },
                  _=>{
                      
                  } 
              }
          },
          Stmt::Set=>{
              
          },
          Stmt::Fix(expr)=>{
              let what=stmts.varnames.last().unwrap().to_string().to_lowercase();

              match expr{
                  Some(ex)=>{
                      operations.push(
                        Operation::Assign(
                          Operand::Var(what),
                          Operand::Exp(ex.clone())
                        )
                      )
                  },
                  None=>{
                    operations.push(
                      Operation::Assign(
                        Operand::Var(what.to_string()),
                        Operand::Par(format!("_{}",what))
                      )
                    )
                  }
              }
          },
          Stmt::Certify(_s,list)=>{
            for c in list{
              let what=c.to_lowercase();
              operations.push(
                Operation::Assign(
                  Operand::Var(what.to_string()),
                  Operand::Par(format!("_{}",what))
                )
              )
            }
          },
          Stmt::Increase(dest,e)=>{
              operations.push(Operation::Inc(
                  Operand::Var(dest.clone()),
                  Operand::Exp(e.clone())
                )
              )
          },
          Stmt::Decrease(dest,e)=>{
              operations.push(Operation::Dec(
                  Operand::Var(dest.clone()),
                  Operand::Exp(e.clone())
                )
              )
          },
          Stmt::Subtract(from,e)=>{
              operations.push(Operation::Sub(
                  Operand::Var(from.clone()),
                  Operand::Exp(e.clone())
                )
              )
          },
          Stmt::Add(to,e)=>{
              operations.push(Operation::Add(
                  Operand::Var(to.clone()),
                  Operand::Exp(e.clone())
                )
              )
          },
          Stmt::Reveal(_to,list)=>{
              let list:Vec<Operand>=list.into_iter().map(|l| 
                Operand::Var(l.to_lowercase())
              ).collect();
              operations.push(Operation::Reveal(list))
          },
          Stmt::Terminate=>{
              operations.push(Operation::Terminate);
          },
          Stmt::Appoint=>{
              let who=stmts.varnames.last().unwrap().to_string().to_lowercase();              
              operations.push(
                Operation::Assign(
                  Operand::Var(who.to_string()),
                  Operand::Par(format!("_{}",who))
                )
              )
          },
          Stmt::May(who,statements)=>{
              //println!("The {} may: ",who);
              ret=false;
              mays.push(who.to_string());
              let mut may_opers:Vec<Operation>=Vec::new();
              vm_stmts(&statements,&variables,&mut parameters,&mut may_opers,&mut mays);
              operations.push(Operation::May(Operand::Var(who.to_string()),may_opers));
              //mays.pop();
          },
          Stmt::If{cond,ontrue,onfalse}=>{
              let mut true_opers:Vec<Operation>=Vec::new();
              let mut false_opers:Vec<Operation>=Vec::new();
              vm_stmts(ontrue,&variables,&mut parameters,&mut true_opers,&mut mays);
              if onfalse.len()>0 {
                vm_stmts(onfalse,&variables,&mut parameters,&mut false_opers,&mut mays);
              }
              operations.push(
                  Operation::If(cond.clone(),true_opers,false_opers)
              )
          },
          Stmt::ClauseCall(clause,params)=>{
              let clause=Operand::Func(clause.to_string());
              let params=params.iter().map(|p|
                  Operand::Var(p.to_lowercase())
              ).collect();
              operations.push(
                  Operation::Call(clause,params)
              )
          },
          Stmt::Sequence=>{

          },
          Stmt::And(_a)=>{
            
          },
          _ => {}
      }
      
  }
  ret
}

fn vm_params_from_expr(expr: &Expression,_variables:&Vec<Variable>,parameters: &mut Vec<Variable>){
    for term in &expr.terms{
        for factor in &term.factors{
            match factor{
                Factor::Sym(sym)=>{
                    let s=sym.sym.to_lowercase();
                    if sym.given && !parameters.iter().any(|x| x.name==s){
                        parameters.push(
                            Variable{
                                name:s,
                                _type:Type::Amount,
                                value:None,
                                bound:false
                            }
                        )
                    }                    
                },
                _=>{}
            }
        }
    }
}

impl Program {
  pub fn deploy(&mut self,_args:Vec<String>,caller:usize){
    self.wallet=Some(caller);
    //c.ctx.variables[c.ctx._self].value=parseInt(caller)
    //run_stmts(c.ctx,args,c.constructor.statements,caller)
  }
  pub fn run(){

  }
}
impl Function {
  pub fn run(){

  }
}
