# ![logo][] Lexon Compiler 0.2 / Rust

[Lexon][] is a program language that anyone can read and that can be used to write readable, legal blockchain smart contracts.

[This][repo] is the source code for its compiler.

### Quick Start

	curl https://sh.rustup.rs -sSf | sh
	source $HOME/.cargo/env
	git clone https://gitlab.com/lexon-foundation/lexon-rust.git
	cd lexon-rust
	cargo build
	cargo run examples/3f.lex


### Pointers

* To get a first idea about Lexon, visit the [Lexon] website.

* To learn to use Lexon, to *write, deploy and manage Lexon contracts,* check out its [documentation][] or try it hands on in the [online editor][] that has examples. You don't need any programming knowledge to understand the examples, and no theory about Lexon either. the online editor allows you to deploy real contracts to real blockchains using the editor, at the click of a button.

* To learn how Lexon itself is made, and to contribute, take a look at the available [resources][] for developers.

More on all that below.


### Disclaimer

Lexon is still heavily in flux, check that the version numbers of documentation, examples and programs that you are using match and do not use it in production as it comes. Version numbers that share the first two digits should be mostly fine to mix (e.g. 0.2.20.4 and 0.2.23.5). The Solidity and Aeternity output of Lexon should at this point always be double checked, bounced, peer-reviewed and audited before using it for anything more relevant. We are crunching real-world examples to improve robustness.

As usual for Open Source, Lexon comes without any warranties.


### Source Code Repository

[This][repo] here is the source code of the rust implementation of the [Lexon][] compiler.

This compiler can turn Lexon code into [Ethereum][] [Solidity][] and [Aeternity][] [Sophia][] code that can be deployed directly to the Ethereum or Aeternity mainnets, or testnets.

This repo also has code that turns the Lexon compiler itself into WebAssembly (WASM), so it can be executed in a browser.

You find the newest version of these sources, online [here][repo].



## Faster Alternatives

This source code and the instructions below describe how to build the Lexon compiler from source code on your computer.

A Lexon user will usually NOT do this but take to one of the following two, faster and more powerful alternatives.

If you want to learn Lexon, you don't need this repository but one of the following.


#### Online Editor and Compiler

*Recommended for quick try out and to deploy to a blockchain: the Lexon [online editor][].*

You can try Lexon, and this compiler - this second, without any installation - using the Lexon [online editor][], where you can write and deploy smart contracts within a minute, for both Ethereum and Aeternity. There is a host of examples ready to check out there. You don't need anything from this repository for this and you don't need to do anything described below.

The Online Editor also makes it very easy to go all the way and deploy your smart contracts to the block chain: i.e. bringing them to life. Everything below is concerned only with the first step of that process, the compilation from Lexon code into the native language of the respective blockchains. The Online Editor handles a second compilation step - of that native language down to a format that the virtual machine of the respective blockchain needs. And it creates the transaction for you that is needed to transport the contract onto the chain.


#### Compiler Binary Download

*Recommended for off-line tinkering with Lexon code: the pre-built Lexon compiler [binaries][].*

Binaries are simply ready-made programs. If you want to try and learn Lexon on your own machine, you can download the [binaries][] of this compiler, for Mac or Linux. Download some [examples][], too.

This can be the right choice if you want to focus on learning to write Lexon contracts, use your own editor or can't be online. It does not afford you a way to get the contract on a blockchain. But if you know how to deploy Solidity or Sophia contracts you will be good. If you don't, don't lose time on that now and use the Lexon [online editor] for that. Or focus on learning Lexon first, using the binaries, and try the deployment later.


## ![lexr logo] The Source

You can use this source code to build the Lexon compiler from scratch, to inspect it, amend it, and contribute. It will help you to make Lexon usable on other OSses than Linux and Mac. Because it is written in [Rust][], the Lexon compiler is very portable and it should be possible to make it run on just about any platform. It has a very small footprint.

The compiler can also run directly in a browser, on most any device, at native speed, i.e. with the feel of an installed application to it: this repository includes the wraps to translate the compiler to [WebAssembly][] (WASM). We use that for the online editor. You don't need the WASM stuff if you wouldn't know what for, it's not needed to build, try and execute the compiler locally on your machine.

This source code would also be the starting point for implementing new natural languages or new blockchain targets. Both is super fun and neither brutally time-consuming nor difficult to get to an experimental stage, if you know a bit of Rust. The *grammar* for controlled English is in `lexon/src/lexon.pest`. The output for Solidity (for Ethereum) is produced in `lexon/src/solidity.rs`. The output for Sophia (for Aeternity) is produced in `lexon/src/solidity.rs`.


###  Compiler – Building & Running


#### Prerequisites

* curl
* git
* gcc
* rust nightly

####  gcc

**Mac**

	brew install gcc


**Ubuntu**

	sudo apt install build-essential


#### Rust nightly

	curl https://sh.rustup.rs -sSf | sh
	source $HOME/.cargo/env


#### Building

	git clone https://gitlab.com/lexon-foundation/lexon-rust.git
	cd lexon-rust
	cargo build

#### Running

	cargo run examples/3f.lex



###  Compiler – WebAssembly Build

The [WebAssembly][] build of the compiler can be inspected live at the [online editor][].


#### Prerequisites

The above and:

* wasm-pack
* npm

#### wasm-pack

	curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

#### npm installation

**Mac**

	brew install node

**Linux**

	curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
	sudo apt-get install -y nodejs

#### Build

	cd lexon-wasm
	./build.sh
	cd pkg
	sudo npm link


[Lexon]:	https://www.lexon.tech				 "Lexon website"
[documentation]:https://www.lexon.tech/docs			 "Lexon user documentation"
[resources]:	https://www.lexon.tech/devs			 "Lexon developer resources"
[repo]:		https://gitlab.com/lexon-foundation/lexon-rust	 "Lexon compiler"
[online editor]:http://3.lexon.tech				 "Lexon editor, online"
[editor]:	https://gitlab.com/lexon-foundation/lexon-editor "Lexon editor source code"
[binaries]:	https://www.lexon.tech/binaries			 "Lexon binaries, i.e. ready-made programs"

[Ethereum]:	https://www.ethereum.org			 "Ethereum website"
[Solidity]:	https://solidity.readthedocs.io			 "Ethereum’s smart contract language"
[Aeternity]:	https://www.aeternity.com			 "Aeternity website"
[Sophia]:	https://github.com/aeternity/aesophia		 "Aeternity’s smart contract language"
[WebAssembly]:	https://webassembly.org				 "WebAssembly virtual machine code website"
[Rust]:		https://rust-lang.org				 "Rust programming language website"

[logo]:		./images/lexon-logo-red-64.svg			 "Lexon logo"
[lexr logo]:	./images/lexon-rust-logo-64.png			 "Lexon rust compiler logo"


