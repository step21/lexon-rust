#!/bin/sh
#wasm-pack build
cargo build --release --target wasm32-unknown-unknown

# update the wasm bindings:
#wasm-bindgen target/wasm32-unknown-unknown/release/lexon_wasm.wasm --no-typescript --out-dir pkg
#wasm-pack build --release --no-typescript
wasm-pack build --release